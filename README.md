# wTorrent

**!! Development in progress : don't use in production**

Wrapper for torrent server compatible with :
- rTorrent
- Transmission

## Usage

```js
const wTorrent = require('wtorrent');
const client = wTorrent({
  client: 'rtorrent' || 'transmission', // One of
  host: '127.0.0.1',
  port: 80,
  endpoint: '/RPC2',
  user: 'user',
  password: 'password',
});

// Get all
await client.get();

//Get one 
await client.getOne(hash);

// Interact
await client.play(hash);
await client.pause(hash);
await client.remove(hash);

//Get files
await client.getFiles(hash);

//Add torrent file
await client.createFromFile(filePath);
await client.createFromBuffer(Buffer)
```

import fs from 'fs';
import path from 'path';

import { Torrent as ITorrent, Tracker as ITracker, File as IFile } from './src/types'
import ITorrentClient from './src/TorrentClientInterface'
import ITorrentOption from './src/TorrentOptionsInterface'
import WTorrentClient from './src/TorrentClientWrapper'

export type Torrent = ITorrent;
export type Tracker = ITracker;
export type File = IFile;
export type TorrentClientInterface = ITorrentClient;
export type TorrentOptionsInterface = ITorrentOption;
export type TorrentClientWrapper = WTorrentClient;

/**
 * @param options
 * @returns {Promise<TorrentClientWrapper>}
 */
const wTorrent: ((options: TorrentOptionsInterface) => Promise<TorrentClientWrapper>) = async(options: TorrentOptionsInterface): Promise<TorrentClientWrapper> => {
  let clientModule = null;
  let client = null;

  if (!options.client) {
    throw new Error('You should give a client in options');
  }

  if (typeof options.client === 'function') {
    if (options.client instanceof WTorrentClient) {
      clientModule = options.client;
    } else {
      throw new Error('You should a client which implement TorrentClientWrapper');
    }
  }

  if (typeof options.client === 'string') {
    if (fs.existsSync(path.join(path.dirname(require.resolve('.')), './src/clients', options.client + '.js'))) {
      clientModule = (await import('./src/clients/' + options.client)).default;
    }
  }

  if (null === clientModule || typeof clientModule !== 'function') {
    throw new Error('Your client does not exists, please use rtorrent or transmission or your own');
  }

  try {
    client = new clientModule(options.host, options.port, options.endpoint, options.user, options.password);
    await client.getVersion();
  } catch (e: any) {
    if (e.message && e.message.startsWith('Unknown XML-RPC')) {
      throw new Error(`Torrent server (${options.name}) is not connected : maybe because you have bad credentials`);
    }
    throw new Error(`Torrent server (${options.name}) is not connected : ${e.message}`);
  }

  if (typeof client.preConfig === 'function') {
    try {
      await client.preConfig();
    } catch (e: any) {
      throw new Error(`Torrent server (${options.name}) preConfig failed : ${e.message}`);
    }
  }

  const wrapper = new WTorrentClient(options, client);
  const clientMethods = Object.getOwnPropertyNames(Object.getPrototypeOf(client));
  const difference = WTorrentClient.getMethodsCompatibility().filter(x => !clientMethods.includes(x));
  if (difference.length > 0) {
    throw new Error(`Your client is not compatible with this version of wTorrent ; client : ${options.client}`);
  }

  return wrapper;
};

export default wTorrent as ((options: TorrentOptionsInterface) => Promise<TorrentClientWrapper>);

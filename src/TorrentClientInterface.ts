import { File, Torrent, Tracker } from './types';

interface TorrentClientInterface {
  getVersion(): Promise<string>;
  get(): Promise<Torrent[]>;
  getFiles(hash: string): Promise<File[]>;
  getTrackers(hash: string): Promise<Tracker[]>;
  getHashes(): Promise<string[]>;
  getOne(hash: string): Promise<Torrent>;
  play(hash: string): Promise<boolean>;
  pause(hash: string): Promise<boolean>;
  start(hash: string): Promise<boolean>;
  stop(hash: string): Promise<boolean>;
  remove(hash: string): Promise<boolean>;
  createFromFile(filePath: string): Promise<boolean>;
  createFromBuffer(buffer: any): Promise<boolean>;
}

export default TorrentClientInterface;

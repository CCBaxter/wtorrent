import { File, Torrent, Tracker } from './types';
import TorrentClientInterface from './TorrentClientInterface';
import TorrentOptionsInterface from './TorrentOptionsInterface';

class TorrentClientWrapper implements TorrentClientInterface {
  name: string;
  client: any;
  torrents: [];
  time?: Date;
  cacheEnabled: boolean;

  constructor(options: TorrentOptionsInterface, client: TorrentClientInterface) {
    this.name = options.name;
    this.client = client;
    this.torrents = [];
    this.time = null;
    this.cacheEnabled = options.cache || false;
  }

  getName(): string {
    return this.name;
  }

  static getMethodsCompatibility(): string[] {
    return ['getVersion', 'get', 'getOne', 'getFiles', 'pause', 'play', 'remove', 'createFromFile', 'createFromBuffer']
  }

  async getVersion(): Promise<string> {
    try {
      return (await this.client.getVersion());
    } catch (e) {
      throw new Error(`Error with "getVersion()", ${e}`);
    }
  }

  async get(): Promise<Torrent[]> {
    try {
      if (!this.cacheEnabled) {
        return this.client.get();
      }

      if (this.time && (new Date()).getTime() < this.time.getTime()) {
        return this.torrents;
      } else {
        this.time = new Date();
        this.time.setSeconds(this.time.getSeconds() + 30);
        this.torrents = (await this.client.get());
        return this.torrents;
      }
    } catch (e) {
      throw new Error(`Error with "get()" ; ${e}`);
    }
  }

  async getHashes(): Promise<string[]> {
    try {
      if (this.time && (new Date()).getTime() < this.time.getTime()) {
        return this.torrents;
      } else {
        this.time = new Date();
        this.time.setSeconds(this.time.getSeconds() + 30);
        this.torrents = (await this.client.getHashes());
        return this.torrents;
      }
    } catch (e) {
      throw new Error(`Error with "getHashes()" ; ${e}`);
    }
  }

  async getOne(hash: string): Promise<Torrent> {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.getOne(hash));
    } catch (e) {
      throw new Error(`Error with "getOne()", hash : ${hash} ; ${e}`);
    }
  }

  async getFiles(hash: string): Promise<File[]> {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.getFiles(hash));
    } catch (e) {
      throw new Error(`Error with "getFiles()", hash : ${hash} ; ${e}`);
    }
  }

  async getTrackers(hash: string): Promise<Tracker[]> {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.getTrackers(hash));
    } catch (e) {
      throw new Error(`Error with "getFiles()", hash : ${hash} ; ${e}`);
    }
  }

  async play(hash: string) {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.play(hash));
    } catch (e) {
      throw new Error(`Error with "play()", hash : ${hash} ; ${e}`);
    }
  }

  async pause(hash: string) {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.pause(hash));
    } catch (e) {
      throw new Error(`Error with "pause()", hash : ${hash} ; ${e}`);
    }
  }

  async start(hash: string) {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.start(hash));
    } catch (e) {
      throw new Error(`Error with "open()", hash : ${hash} ; ${e}`);
    }
  }

  async stop(hash: string) {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.stop(hash));
    } catch (e) {
      throw new Error(`Error with "close()", hash : ${hash} ; ${e}`);
    }
  }

  async remove(hash: string) {
    if (!hash) {
      throw new Error('"hash" is missing');
    }

    try {
      return (await this.client.remove(hash));
    } catch (e) {
      throw new Error(`Error with "remove()", hash : ${hash} ; ${e}`);
    }
  }

  async createFromBuffer(buffer: any) {
    if (!buffer) {
      throw new Error('"buffer" is missing');
    }

    try {
      return (await this.client.createFromBuffer(buffer));
    } catch (e: any) {
      throw new Error(`Error with "createFromBuffer()" ; ${e.message}`);
    }
  }

  async createFromFile(file: string) {
    if (!file) {
      throw new Error('"file" is missing');
    }

    try {
      return (await this.client.createFromFile(file));
    } catch (e: any) {
      throw new Error(`Error with "createFromFile()" ; ${e.message}`);
    }
  }
}

export default TorrentClientWrapper;

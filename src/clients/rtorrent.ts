import xmlrpc from 'xmlrpc';
import fs from 'fs';
import TorrentClientInterface from '../TorrentClientInterface';
import { File, Torrent, Tracker } from '../types';

class Client implements TorrentClientInterface {
  client: any;
  downloadPath: string;
  private defaultParams = [
    'd.hash=', 'd.name=', 'd.base_path=', 'd.ratio=', 'd.tied_to_file=',
    'd.timestamp.started=', 'd.timestamp.finished=', 'd.creation_date=', 'd.state_changed=', 'd.is_open=',
    'd.is_active=', 'd.complete=', 'd.size_bytes=', 'd.completed_bytes=', 'd.up.total=',
    'd.down.rate=', 'd.up.rate=', 'd.free_diskspace=', 'd.peers_connected=', 'd.peers_complete=',
    'd.load_date=', 'd.custom=addtime'
  ];

  constructor(host: string, port?: number, endpoint?: string, user?: string, password?: string) {
    this.client = xmlrpc.createClient({
      host,
      port,
      path: endpoint || '/RC2',
      basic_auth: {
        user: user || null,
        pass: password || null,
      },
    });
  }

  async preConfig(): Promise<void> {
    this.downloadPath = await this.call('directory.default', []);
  }

  async getVersion() {
    return `rTorrent - v${(await this.call('system.client_version'))}`;
  }

  async getHashes(): Promise<string[]> {
    return this.call('download_list', []);
  }

  async get(): Promise<Torrent[]> {
    const torrents = await this.call(
      'd.multicall2',
      ['', '',].concat(this.defaultParams)
    );

    return Promise.all(torrents.map((torrent: string[]) => this.formatTorrentFromMultiCall(torrent)));
  }

  async getOne(hash: string): Promise<Torrent> {
    const torrents = await this.call(
      'd.multicall.filtered',
      ['', '', `equal=cat=$d.hash=,cat=${hash}`].concat(this.defaultParams)
    );

    if (torrents.length === 0) {
      throw new Error(`This torrent does not exits : ${hash}`);
    }

    return this.formatTorrentFromMultiCall(torrents[0])
  }

  async getTrackers(hash: string): Promise<Tracker[]> {
    return (await this.call(
      't.multicall',
      [hash, '', 't.url=', 't.is_enabled=', 't.is_open=', 't.type=', 't.scrape_complete=', 't.scrape_incomplete=', 't.scrape_downloaded=',]
    ))
      .map((tracker: string[]) => ({
        url: tracker[0],
        isEnabled: tracker[1] === '1',
        isOpen: tracker[2] === '1',
        type: Number(tracker[3]),
        seeders: parseInt(tracker[4], 10),
        leechers: parseInt(tracker[5], 10),
        numberDownloaded: parseInt(tracker[6], 10),
      }));
  }

  async getFiles(hash: string): Promise<File[]> {
    return (await this.call('f.multicall', [hash, '', 'f.path=', 'f.size_bytes=', 'f.completed_chunks=', 'f.size_chunks=',]))
      .map((file: string[]) => ({
        name: file[0],
        length: parseInt(file[1], 10),
        isCompleted: file[2] === file[3],
      }));
  }

  async start(hash: string) {
    return this.call('d.start', [hash]);
  }

  async stop(hash: string) {
    return this.call('d.stop', [hash]);
  }

  async play(hash: string) {
    return this.call('d.resume', [hash]);
  }

  async pause(hash: string) {
    return this.call('d.pause', [hash]);
  }

  async remove(hash: string) {
    return this.call('d.erase', [hash]);
  }

  async createFromFile(filePath: string) {
    const content = fs.readFileSync(filePath);
    return this.call('load.raw_start', ['', content]);
  }

  async createFromBuffer(buffer: any) {
    return this.call('load.raw_start', ['', buffer]);
  }

  private call(method: string, params: any[] = []): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client.methodCall(method, params, (error: any, result: any) => {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      });
    });
  }

  private async formatTorrentFromMultiCall(torrent: string[]): Promise<Torrent> {
    const trackers = await this.getTrackers(torrent[0]);
    const files = await this.getFiles(torrent[0]);

    return {
      hash: torrent[0],
      name: torrent[1],
      path: torrent[2],
      ratio: parseInt(torrent[3], 10) / 1000,
      torrentFile: torrent[4],
      startedAt: (new Date(parseInt(torrent[5], 10) * 1000)).toJSON(),
      finishedAt: (new Date(parseInt(torrent[6], 10) * 1000)).toJSON(),
      createdAt: (new Date(parseInt(torrent[7], 10) * 1000)).toJSON(),
      stateChangedAt: (new Date(parseInt(torrent[8], 10) * 1000)).toJSON(),
      loadedAt: (new Date(parseInt(torrent[5], 10) * 1000)).toJSON() || (new Date(parseInt(torrent[7], 10) * 1000)).toJSON(),
      isOpen: torrent[9] === '1',
      isActive: torrent[10] === '1',
      isComplete: torrent[11] === '1',
      length: parseInt(torrent[12], 10),
      downloaded: parseInt(torrent[13], 10),
      uploaded: parseInt(torrent[14], 10),
      downRate: parseInt(torrent[15], 10),
      upRate: parseInt(torrent[16], 10),
      freeSpace: parseInt(torrent[17], 10),
      peersConnected: parseInt(torrent[18], 10),
      peersCompleted: parseInt(torrent[19], 10),
      totalSeeders: trackers.reduce((prev, next) => prev + next.seeders, 0),
      totalLeechers: trackers.reduce((prev, next) => prev + next.leechers, 0),
      trackers,
      files,
    };
  }
}

export default Client;

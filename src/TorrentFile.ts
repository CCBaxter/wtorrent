interface TorrentFile {
  name: string,
  isCompleted: boolean,
  length: number,
}

export default TorrentFile;

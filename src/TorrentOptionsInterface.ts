interface TorrentOptionsInterface {
  client: string | (() => void),
  name: string,
  host: string,
  port?: number,
  endpoint?: string,
  user?: string,
  password?: string,
  cache?: boolean,
}

export default TorrentOptionsInterface
